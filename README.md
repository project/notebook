# Notebook module

Notebook module is created for enhancing functionality of Drupal. It provides admin or user to add memory or notes in busy circumstances.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/notebook).

## Installation

Visit above link and copy path to install through composer

## Usage

- First install notebook module after installing cleare cache and refresh site. It apear in admin menu as mention name Notebook. Click this link and see Mynotebook page to use this module.

## Requirements

This module requires no any other module dependency

## Maintainers

- Name Radheshyam Ahirwar
- email radhe.rathore10@gmail.com
- git username radhe_shyam

## Issues

If you encounter any issues or have suggestions for improvements, please open an issue in the [module's issue queue](https://www.drupal.org/project/issues/notebook).

## License

This module is licensed under the [GNU General Public License](https://www.gnu.org/licenses/gpl-2.0.html).

## Credits

This module was created by Radheshyam Ahirwar working in Smashing infolabe pvt. ltd. Indore.

## Changelog

- This module is compatible for drupal 10 and also in drupal 8 and drupal 9.
