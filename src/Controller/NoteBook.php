<?php

namespace Drupal\notebook\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Render\Markup;

/**
 * Class Display.
 *
 * @package Drupal\notebook\Controller
 */
class NoteBook extends ControllerBase {

  /**
   * NoteBook Page.
   *
   * @return string
   *   Return Table format data.
   */
  public function noteBookPage() {
    $id = "notebook-page";
    $form = $this->formBuilder()->getForm('\Drupal\notebook\Form\addNoteForm');
    $myController = \Drupal::classResolver()
      ->getInstanceFromDefinition('\Drupal\notebook\Controller\displayNotes');
    $renderedController = $myController->showNotes();
    $renderForm = \Drupal::service('renderer')->render($form);
    $renderNotes = \Drupal::service('renderer')->render($renderedController);
    $output1 = [
      '#markup' => Markup::create($renderForm),
    ];
    $output2 = [
      '#markup' => Markup::create($renderNotes),
    ];
    return [
      '#theme' => 'notebook_page',
      '#output1' => $output1,
      '#output2' => $output2,
      '#output_id' => $id,
    ];

  }

}
