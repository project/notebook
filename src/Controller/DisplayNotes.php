<?php

namespace Drupal\notebook\Controller;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;

/**
 * Class Display.
 *
 * @package Drupal\notebook\Controller
 */
class DisplayNotes extends ControllerBase {

  /**
   * Show data.
   *
   * @return string
   *   Return Table format data.
   */
  public function showNotes() {
    $build['#attached']['library'][] = 'notebook/notebook';
    $result = "";
    $select = \Drupal::database()->select('notebook_table', 'n');
    $select->Fields('n');
    $pager = $select->extend('Drupal\Core\Database\Query\PagerSelectExtender')
      ->limit(4);
    $result = $pager->execute()->fetchAll();
    if ($result != NULL) {
      $rows = [];
      foreach ($result as $row) {
        $date_time = $row->created;
        $timestamp = strtotime($date_time);
        $date = date('F j, Y', $timestamp);
        $url_show = Url::fromRoute('see_note_page', ['show_id' => $row->notes_id], []);
        $linkShow = Link::FromTextAndUrl('View', $url_show);
        $delete = new FormattableMarkup('<a href="javascript:void(0);" class = "delete-note" id = "delete-note" data-id = "' . $row->notes_id . '" >Remove</a>', []);
        $linkDelete = t('@deletelink', ['@deletelink' => $delete]);
        $rows[] = [
          'data' => [
            'notes_id' => $row->notes_id,
            'subject' => $row->subject,
            'created' => $date,
            'show' => $linkShow,
            'delete' => $linkDelete,
          ],
        ];
      }

      $build['table'] = [
        '#type'  => 'table',
        '#rows' => $rows,
        '#empty' => $this->t('No data available'),
        '#attributes' => [
          'class' => ['table'],
        ],
        '#prefix' => '<div class="table-responsive">',
        '#suffix' => '</div>',
      ];
      $build['pager'] = [
        '#type' => 'pager',
      ];
      // Render the table using the render() function.
      $table_output = \Drupal::service('renderer')->render($build);
      // Create an array for your output.
      $output = [
        '#markup' => Markup::create($table_output),
      ];
      return [
        $output,
      ];

    }
    else {
      $output = [
        '#markup' => Markup::create("<div class = 'message'>No records found.</div>"),
      ];
      return $output;
    }
  }

}
