<?php

namespace Drupal\notebook\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class Display.
 *
 * @package Drupal\notebook\Controller
 */
class RemoveNote extends ControllerBase {

  /**
   * Removing notes.
   *
   * @return string
   *   Return Table format data.
   */
  public function removeThisNote() {
    $note_id = $_GET['id'];
    $result = "";
    $delete = \Drupal::database()->delete('notebook_table');
    $delete->condition('notes_id', $value = $note_id, $operator = '=');
    $result = $delete->execute();
    $response = new RedirectResponse(Url::fromRoute('notebook.page')->toString());
    return $response;

  }

}
