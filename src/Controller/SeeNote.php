<?php

namespace Drupal\notebook\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Class Display.
 *
 * @package Drupal\notebook\Controller
 */
class SeeNote extends ControllerBase {

  /**
   * Showdata.
   *
   * @return string
   *   Return Table format data.
   */
  public function seeThisNote() {
    $id = "note-page";
    $note_id = $_REQUEST['show_id'];
    $build['#attached']['library'][] = 'notebook/notebook';
    $result = "";
    $select = \Drupal::database()->select('notebook_table', 'n');
    $select->Fields('n');
    $select->condition('notes_id', $value = $note_id, $operator = '=');
    $result = $select->execute()->fetchAll();
    if ($result != NULL) {
      $rows = [];
      $output = [];
      foreach ($result as $row) {
        $url_edit = Url::fromRoute('edit_not_controller', ['edit_id' => $row->notes_id], []);
        $linkEdit = Link::FromTextAndUrl('Edit', $url_edit);
        $output[] = [
          '#markup' => '<table class="table"><tr><td><i>Subject  </i></td><td>' . $row->subject . '</td></tr><tr><td><i>Body</t></td><td>' . $row->description . '</td></tr><tr><td><i>Phone Number  </i></td><td>' . $row->phone_number . '</td></tr><tr><td><i>Date  </i></td><td>' . $row->created . '</td></tr></table>',
        ];
      }

      $build['output'] = $output;
      $build['link'] = [
        'my_link' => [
          '#type' => 'link',
          '#title' => $linkEdit->getText(),
          '#url' => $linkEdit->getUrl(),
          '#attributes' => [
            'id' => ['edit-submit'],
            'class' => ["btn btn-success button--primary button js-form-submit form-submit"],
          ],
        ],
      ];

      return [
        '#theme' => 'notebook_page',
        '#output1' => $build,
        '#output_id' => $id,
      ];

    }

  }

}
