<?php

namespace Drupal\notebook\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Render\Markup;

/**
 * Class Display.
 *
 * @package Drupal\notebook\Controller
 */
class EditNoteFormDisplay extends ControllerBase {

  /**
   * Edit Form Display.
   *
   * @return string
   *   Return table data Editable .
   */
  public function editNote() {
    $id = "edit-note";
    $form = $this->formBuilder()->getForm('\Drupal\notebook\Form\editNoteForm');
    $renderForm = \Drupal::service('renderer')->render($form);
    $output1 = [
      '#markup' => Markup::create($renderForm),
    ];
    return [
      '#theme' => 'notebook_page',
      '#output1' => $output1,
      '#output_id' => $id,
    ];

  }

}
