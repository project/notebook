<?php

namespace Drupal\notebook\Form;

use Drupal\Core\Database\Database;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class for add note form.
 *
 * {@inheritdoc}
 */
class AddNoteForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'add_note_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#attached']['library'][] = 'notebook/notebook';
    $form['subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Enter subject'),
      '#required' => TRUE,
      '#size' => 200,
      '#maxlength' => 200,
    ];
    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Write Notes'),
      '#required' => TRUE,
      '#size' => 500,
      '#maxlength' => 500,
    ];
    $form['phone_number'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Contact number'),
      '#size' => 10,
      '#maxlength' => 10,
      '#element_validate' => ['notebook_validate_phone_number'],
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add'),
      '#attributes' => [
        'class' => ['btn', 'btn-success'],
      ],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    try {
      $field['subject'] = $form_state->getValue(['subject']);
      $field['description'] = $form_state->getValue(['description']);
      $field['phone_number'] = $form_state->getValue(['phone_number']);
      $conn = Database::getConnection();
      $conn->insert('notebook_table')
        ->fields($field)->execute();
      \Drupal::messenger()->addMessage($this->t('The note has been succesfully saved'));
    }
    catch (Exception $ex) {
      \Drupal::logger('notebook')->error($ex->getMessage());
    }
  }

}
