<?php

namespace Drupal\notebook\Form;

use Drupal\Core\Database\Database;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * This is a brief description of the class.
 *
 * Class EditNoteForm.
 */
class EditNoteForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'edit_note_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#attached']['library'][] = 'notebook/notebook';
    $note_id = $_REQUEST['edit_id'];
    $sql = \Drupal::database()->select('notebook_table', 't');
    $sql->fields('t');
    $sql->condition('notes_id', $value = $note_id, $operator = '=');
    $result = $sql->execute()->fetchAll();
    if ($result === NULL) {
      \Drupal::messenger()->addMessage($this->t('No records founds'));
    }
    else {
      foreach ($result as $row) {
        $form['subject'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Edit subject'),
          '#required' => TRUE,
          '#size' => 200,
          '#maxlength' => 200,
          '#default_value' => $row->subject,
        ];

        $form['description'] = [
          '#type' => 'textarea',
          '#title' => $this->t('Edit body'),
          '#required' => TRUE,
          '#size' => 500,
          '#maxlength' => 500,
          '#default_value' => $row->description,
        ];

        $form['phone_number'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Edit Contact number'),
          '#size' => 10,
          '#maxlength' => 10,
          '#element_validate' => ['notebook_validate_phone_number'],
          '#default_value' => $row->phone_number,
        ];

        $form['actions']['submit'] = [
          '#type' => 'submit',
          '#value' => $this->t('Save'),
          '#attributes' => [
            'class' => ['btn', 'btn-success'],
          ],
        ];
      }

      return $form;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    try {
      $note_id = $_REQUEST['edit_id'];
      $subject = $form_state->getValue(['subject']);
      $description = $form_state->getValue(['description']);
      $phone_number = $form_state->getValue(['phone_number']);
      $conn = Database::getConnection();
      $updated = $conn->update('notebook_table')
        ->fields([
          'subject' => $subject,
          'description' => $description,
          'phone_number' => $phone_number,
        ])
        ->condition('notes_id', $note_id)
        ->execute();
      if ($updated) {
        \Drupal::messenger()->addMessage($this->t('The note data has been successfully updated'));
        $form_state->setRedirect('see_note_page', ['show_id' => $note_id]);
      }
      else {
        \Drupal::messenger()->addMessage($this->t('Try again later'));
      }
    }
    catch (Exception $ex) {
      \Drupal::logger('notebook')->error($ex->getMessage());
    }
  }

}
