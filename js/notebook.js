(function ($, Drupal, drupalSettings) {
    Drupal.behaviors.notebook = {
      attach: function (context, settings) {
      //  $('.delete-note', context).once('#delete-note').click(function (e) // for drupal 9
       $(once('#delete-note', '.delete-note', context)).click(function (e) {
          var path = Drupal.url('remove/note');
          var $this = $(this);
          var id = $this.data('id');
          var message = "Do you want to delete";
          if (confirm(message)) {
              $.ajax({
                  type: "GET",
                  data:{id:id},
                  url:path,
                  success: function (response) {
                      $('#send').html(data);
                  }
              })
          }
          location.reload();
      });
      }
    };
  })(jQuery, Drupal, drupalSettings);
